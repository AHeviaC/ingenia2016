
#include "stdafx.h"
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/AlphaFunc>
#include <osg/Texture2D>
#include <osgDB/readFile>


osg::Geode* CreaMalla(int num_puntos,double separacion_puntos);

void AddTexture(osg::Node* node, std::string nombre_imagen, int unidad_textura);

void AddShader(osg::Node* node,std::string vertex_shader,std::string fragment_shader);

float obtenerAltura(float posX, float posY, osg::Image* imagen_heightmap, int num_puntos, double separacion_puntos, double altura_total);
