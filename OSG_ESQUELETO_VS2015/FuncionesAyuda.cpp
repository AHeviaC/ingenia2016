#include "stdafx.h"		
#include "FuncionesAyuda.h"	

float obtenerAltura(float posX, float posY,osg::Image* imagen_heightmap, int num_puntos,double separacion_puntos,double altura_total)
{
	double tamMalla = num_puntos*separacion_puntos; 
	
	double xGeom = (posX + (tamMalla/2))/(tamMalla);
	double yGeom = (posY + (tamMalla/2))/(tamMalla);

	double posZ = imagen_heightmap->getColor(osg::Vec2(xGeom, yGeom)).r()*altura_total;
	return posZ;
}


osg::Geode* CreaMalla(int num_puntos,double separacion_puntos)
{
	osg::Geometry* geom = new osg::Geometry;
	osg::Geode* geode = new osg::Geode;
	geode->addDrawable(geom);
	
	osg::Vec3Array* v = new osg::Vec3Array;
	osg::Vec2Array* texcoords1 = new osg::Vec2Array(); //Vectores de coordenadas
	osg::Vec2Array* texcoords2 = new osg::Vec2Array();
	for( int y = -num_puntos/2; y < num_puntos/2; y++)
	{
		for( int x = -num_puntos/2; x < num_puntos/2; x++)
			{
				double a = double(x*separacion_puntos);
				double b =  double(y*separacion_puntos);
				v->push_back(osg::Vec3(a,b,0)); //Push_back es como la funci�n que a�ad�a al final de una lista
				texcoords1->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1))); //Algoritmo para traducir a coordenadas
				texcoords2->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1)));
			}
		}
	


	geom->setVertexArray(v);
	geom->setTexCoordArray(0,texcoords1);
	geom->setTexCoordArray(1,texcoords2);


	osg::DrawElementsUInt* elements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);
	for(unsigned int y=0;y<num_puntos-1;y++)
	{
		for(unsigned int x=0;x<num_puntos-1;x++)
		{
			elements->push_back((y+1)*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x+1);
			elements->push_back((y+1)*(num_puntos)+x+1);
		}
	}
	
	
	geom->addPrimitiveSet(elements);
	return geode;
}

void AddTexture(osg::Node* node, std::string nombre_imagen, int unidad_textura)
{
	const char *vinit[] = {"texUnit0","texUnit1","texUnit2","texUnit3","texUnit4","texUnit5","texUnit6","texUnit7"};
	osg::Uniform*	t = new osg::Uniform(vinit[unidad_textura],unidad_textura); //Creaci�n de uniforms, cuidado de no pisar estos al hacer uno manual


	node->getOrCreateStateSet()->addUniform(t);

	
	
	osg::Image* image = osgDB::readImageFile( nombre_imagen);	//Lectura imagen
	osg::Texture2D* textura = new osg::Texture2D(image);
	textura->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR_MIPMAP_LINEAR);
	textura->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
	textura->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::REPEAT); //Esto era CLAMP, temas de bordes, repasar llegado el momento, creo que por eso vuelve a subir fuera del heightmap
	textura->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::REPEAT);

	
	node->getOrCreateStateSet()->setTextureAttributeAndModes(unidad_textura,textura,osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE | osg::StateAttribute::PROTECTED);
	
	osg::AlphaFunc* alphaFunc = new osg::AlphaFunc;
    alphaFunc->setFunction(osg::AlphaFunc::GEQUAL,0.05f);
	node->getOrCreateStateSet()->setAttributeAndModes( alphaFunc, osg::StateAttribute::ON );

}

void AddShader(osg::Node* node,std::string vertex_shader,std::string fragment_shader) //el vetex tiene uniform (directamente desde el c, el TwxtUnit0)
																					  // varging float z, guarda el tipo de variable de las coordenadas interpoladas (CREO) y sirven para pasar del vertex al fragment
	//uniform textCoord textUnit0       varging float z
{
	osg::Program* ProgramObject = new osg::Program;

	osg::Shader* VertexObject =   new osg::Shader( osg::Shader::VERTEX );
	VertexObject->loadShaderSourceFromFile(vertex_shader);

	osg::Shader* FragmentObject = new osg::Shader( osg::Shader::FRAGMENT);
	FragmentObject->loadShaderSourceFromFile(fragment_shader);

	ProgramObject->addShader(FragmentObject) && ProgramObject->addShader(VertexObject);

	node->getOrCreateStateSet()->setAttributeAndModes(ProgramObject, osg::StateAttribute::ON | osg::StateAttribute::PROTECTED);

}
/* gl_position guarda las coordenadas de la ftransform (que calcula las coordendas en pantalla de un vertice en un plano)
gl_position=gl_ModelViewProjectionMatrix*gl_Vertex; Aquin o esta incluida la z (en gl_vertex) por lo que no se usa este
vec4.altura= Texture2D (TextUnit0, gl_TextCoord[0].st); obtuiene el valor de la textura en unas coordenadas, en nuestro caso ser�a tectUnit0
gl_TextCoord[0] = gl_MultiTexCoord0 ;
vec4 posicion = gl_Vertex, Vec4 es una declaracion de variable qeu se llama posicion
poicion.z = altura.r(de rgb, que valen lo mismo porq en la altura estan en escala de grises)*"AlturaMaxima"(float)+"AlturaMin"(si no fuese cero);
gl_position=gl_ModelViewProjectionMatrix*posicion; aqui tenemos el vector posicion (del tipo vec4) con x,y,z que queremos

Fragment

uniform TextCood TextUnit1
varging float z (tiene las cotas de altura calculado anteriormente)

void main()

vec4 ColorRGB = Texture2D(TextUnit1, gl_TextCoord[0].st) Habria que poner el 1, pero resulta ser el mismo
ColorRGB.r = texture2d (textunit2,256*gl_textcoord[0].st) o algo asi, para que se repita la textura en vez de aplixcar una sola expandidda


OPCIONAL
Calcular las normales y pasarla con un varging para dar una mejor sensaci�n visual (por ejemplo luz)

*/