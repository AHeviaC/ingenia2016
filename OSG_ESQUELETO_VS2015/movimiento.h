#pragma once
//#include <osg/MatrixTransform>
//#include <osgViewer/Viewer>
//#include <osg/LineSegment>
//#include <osgUtil/IntersectVisitor>	
#include "stdafx.h"
#include "MyKeyboardEventHandler.h"
#define PI 3.14159265358979323846
class movimiento
{
protected:
	osg::Matrixd mtras;
	osg::Matrixd mrotz;
	osg::Matrixd mesc;
	osg::Matrixd mtotal;
	
	osg::Quat quat;
	
	osg::LineSegment* LocationSegment = new osg::LineSegment();
	osg::LineSegment* LocationSegment1 = new osg::LineSegment();
	osgUtil::IntersectVisitor::HitList ElevationLocatorHits;

public:
	osg::Vec3d alturaLineSegment;
	osg::Vec3d Vec;
	osg::Vec3d Pos;
	osg::Vec3d altura_camara;
	float ang, angulo_giro, vel, vel1, vel2, a, tiempop, alfa, longitud_salto, altura_salto;
	int n, space, space1, pos, distancia_camara,carcel;
	osg::Group* _grupo;
	movimiento();
	~movimiento();
	void muevete(MyKeyboardEventHandler* handler, osg::MatrixTransform* personaje, osgViewer::Viewer* _viewer,float tiempo);
	void colisiones(osg::Vec3d &Pos, osg::MatrixTransform* &personaje, osgViewer::Viewer* _viewer, osg::Quat quat, float ang, float tiempo);
};

