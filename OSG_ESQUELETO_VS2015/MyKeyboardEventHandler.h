#pragma once
#include "stdafx.h"
class MyKeyboardEventHandler : public osgGA::GUIEventHandler {

protected:
	//osg::MatrixTransform* _driveCar;
	//friend void movimiento::muevete(MyKeyboardEventHandler* handler);
public:
	int w, s, a, d, q, e, f, shift, space, inicio, flecha_arriba, flecha_abajo, enter;
	MyKeyboardEventHandler() : osgGA::GUIEventHandler() {
		w = 0;
		s = 0;
		a = 0;
		d = 0;
		q = 0;
		e = 0; 
		f = 0; 
		shift = 0;
		space = 0;
		inicio = 0;
		flecha_arriba = 0;
		flecha_abajo = 0;
		enter = 0;
	
	}

	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa,
		osg::Object* obj, osg::NodeVisitor* nv) {

		/*osg::LineSegment* tankLocationSegment = new osg::LineSegment();
		osgUtil::IntersectVisitor findTankElevationVisitor;
		osgUtil::IntersectVisitor::HitList tankElevationLocatorHits;*/

		switch (ea.getEventType()) {

		case osgGA::GUIEventAdapter::KEYDOWN: {
			
			switch (ea.getKey()) {

			case osgGA::GUIEventAdapter::KEY_W:
				w = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_S:
				s = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_A:
				a = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_D:
				d = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Q:
				q = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_E:
				e = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Shift_L:
				shift = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Space:
				space = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Home:
				inicio = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_F:
				f = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Up:
				flecha_arriba = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Down:
				flecha_abajo = 1;
				return true;
			case osgGA::GUIEventAdapter::KEY_Return:
				enter = 1;
				return true;
			}

		default: return false;
		}
		case osgGA::GUIEventAdapter::KEYUP: {

			switch (ea.getKey()) {

			case osgGA::GUIEventAdapter::KEY_W:
				w = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_S:
				s = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_A:
				a = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_D:
				d = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Q:
				q = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_E:
				e = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Shift_L:
				shift = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Space:
				space = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Home:
				inicio = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_F:
				f = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Up:
				flecha_arriba = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Down:
				flecha_abajo = 0;
				return true;
			case osgGA::GUIEventAdapter::KEY_Return:
				enter = 0;
				return true;
			}
		}


		}
	}
};