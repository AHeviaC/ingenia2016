#include "stdafx.h"
#include "AudioItemChunk.h"


AudioItemChunk::AudioItemChunk()
{
	printf("constructor vacio\n");
}

AudioItemChunk::AudioItemChunk(std::string p)
{
	error = 0;
	path = p;
	wave = Mix_LoadWAV(path.c_str());
	if (wave == NULL)
		error = 1;
	else
		printf("audio reproduciendo: %s\n", path.c_str());
}


AudioItemChunk::~AudioItemChunk()
{
	Mix_FreeChunk(wave);
}

void AudioItemChunk::play( int veces)
{
	
	if (veces >= 0) { //si veces es positivo hay que restarle uno para que el sonido se repita (veces-1) veces
		veces--;
	
	}
	else
		veces = -1; //si veces es negativo o cero se repetir� infinitas veces
	if (Mix_PlayChannel(-1, wave, veces) == -1) //�ltimo argumento: veces que se repite (-1=infinitas veces)
		error = 2;
	;
	
}

