#pragma once

#include "SDL.h"
#include "SDL_mixer.h"

#include "AudioItem.h"
#include <list>

class AudioGeneral
{
private:
	unsigned int error;
	unsigned int frecuencia;
	unsigned int canales;
	std::list<AudioItem> listaChunks;
	std::list<AudioItem> listaMusica;
public:
	AudioGeneral();
	~AudioGeneral();
	unsigned int getError() { return error; }
	void addChunk(AudioItem ai);
	void addMusica(AudioItem ai);
	void playChunk(std::string name, int n);
	void playMusic(std::string name, int n);
};

