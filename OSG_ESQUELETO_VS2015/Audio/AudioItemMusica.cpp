#include "stdafx.h"
#include "AudioItemMusica.h"


AudioItemMusica::AudioItemMusica()
{
	printf("constructor vacio\n");
}

AudioItemMusica::AudioItemMusica(std::string p)
{
	error = 0;
	path = p;
	music = Mix_LoadMUS(path.c_str());
	if (music == NULL) {
		error = 1;
		printf("ERROR al crear audio: %s\n", path.c_str());
	}
	else
		printf("audio creado: %s\n", path.c_str());
}


AudioItemMusica::~AudioItemMusica()
{
	Mix_FreeMusic(music);
}

void AudioItemMusica::play(int veces)
{
	if( Mix_PlayMusic(music, veces)==-1 ) //�ltimo argumento: veces que se repite (-1=infinitas veces)
		error = 2;
}

