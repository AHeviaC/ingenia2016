#include "movimiento.h"
#include "stdafx.h"	



movimiento::movimiento()
{
	Pos = osg::Vec3d(-31, 160, 0);
	Vec = osg::Vec3d(1, 0, 0);
	alturaLineSegment = osg::Vec3d(0, 0, 3);
	mesc = mesc.scale(1, 1, 1);
	mrotz = mrotz.rotate(PI / 2, osg::Vec3d(0, 0, 1));
	ang = PI / 2;
	a = 0;
	vel = 0.01;
	vel1 = 0.01;
	quat = osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), 0, osg::Vec3d(0, 0, 1));
	tiempop = 0.1;
	n = 0;
	space = 0;
	space1 = 0;
	alfa = PI / 4;
	pos = 0;
	distancia_camara = 20;
	altura_camara = osg::Vec3d(0, 0, 1);
	longitud_salto = 0.15;
	altura_salto = 0.3;
	angulo_giro = 320;
	//findElevationVisitor->setTraversalMask()
}


movimiento::~movimiento()
{
}

void movimiento::colisiones(osg::Vec3d &Pos, osg::MatrixTransform* &personaje, osgViewer::Viewer* _viewer, osg::Quat quat, float ang, float tiempo) {
	osgUtil::IntersectVisitor* findElevationVisitor = new osgUtil::IntersectVisitor;
	LocationSegment->set(Pos + alturaLineSegment, Pos + alturaLineSegment + quat*Vec);

	findElevationVisitor->addLineSegment(LocationSegment);
	_grupo->accept(*findElevationVisitor);

	ElevationLocatorHits = findElevationVisitor->getHitList(LocationSegment);
	if (ElevationLocatorHits.empty()) {
		mtras = mtras.translate(Pos + quat*Vec*tiempo*vel + quat*Vec*0.5*a*tiempo*tiempo);
		mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
		mtotal = mesc*mrotz*mtras;
		Pos = Pos + quat*Vec*tiempo*vel + quat*Vec*0.5*a*tiempo*tiempo;
		personaje->setMatrix(mtotal);
		_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));

	}
	else
	{
		//osgUtil::Hit p = ElevationLocatorHits.front();
		//osg::Vec3d terrainHeight = p.getWorldIntersectPoint();
		//printf("%f",terrainHeight.x());
		//return true;
	}

}

void movimiento::muevete(MyKeyboardEventHandler* handler, osg::MatrixTransform* personaje, osgViewer::Viewer* _viewer, float tiempo) {
	if (handler->shift == 1) { vel = 2 * vel1; }
	else vel = vel1;
	if (Pos._v[0] >= 40 && Pos._v[0] <= 80 && Pos._v[1] >= -115 && Pos._v[1] <= -75) pos = 1;
	if (Pos._v[0] >= 50 && Pos._v[0] <= 65 && Pos._v[1] >= -100 && Pos._v[1] <= -90) pos = 2;
	if (handler->w == 1 && space==0) {
		if (handler->a == 1) {
			colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), PI / 4, osg::Vec3d(0, 0, 1)), ang + PI / 4, tiempo);	
		}
		else if (handler->d == 1) {
			colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), -PI / 4, osg::Vec3d(0, 0, 1)), ang - PI / 4, tiempo);
		}
		else {
			colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), 0, osg::Vec3d(0, 0, 1)), ang, tiempo);
		}
	}
	else if (handler->s == 1 && space==0) {
		colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), PI, osg::Vec3d(0, 0, 1)), ang, tiempo);
	}
	else if (handler->a == 1 && space==0) {
		colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), PI / 2, osg::Vec3d(0, 0, 1)), ang + PI / 2, tiempo);
	}
	else if (handler->d == 1 && space==0) {
		colisiones(Pos, personaje, _viewer, osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), -PI / 2, osg::Vec3d(0, 0, 1)), ang - PI / 2, tiempo);
	}
	else if ((handler->space == 1 && space1==0)||space==1) {
			space = 1;
			if (n <= 10) {
				mtras = mtras.translate(Pos + osg::Vec3d(0, 0, 0.2));
				mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
				mtotal = mesc*mrotz*mtras;
				personaje->setMatrix(mtotal);
				Pos = Pos + osg::Vec3d(0, 0, 0.2);
			}
			else {
				mtras = mtras.translate(Pos + osg::Vec3d(0, 0, -0.2));
				mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
				mtotal = mesc*mrotz*mtras;
				personaje->setMatrix(mtotal);
				Pos = Pos + osg::Vec3d(0, 0, -0.2);
			}
			n = n + 1;
			if (n == 22) {
				n = 0; space=0;
			}

	}
	if (((handler->space == 1 && (handler->w==1||handler->a==1||handler->d==1)) && space==0) || space1 == 1) {
			space1 = 1;
			osgUtil::IntersectVisitor* findElevationVisitor = new osgUtil::IntersectVisitor;
			LocationSegment->set(Pos, Pos + Vec);

			findElevationVisitor->addLineSegment(LocationSegment);
			_grupo->accept(*findElevationVisitor);

			ElevationLocatorHits = findElevationVisitor->getHitList(LocationSegment);
			if (ElevationLocatorHits.empty())
			{
				mtras = mtras.translate(Pos + Vec*cos(alfa)*longitud_salto+ osg::Vec3d(0, 0, sin(alfa)*altura_salto));
				mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
				mtotal = mesc*mrotz*mtras;
				Pos = Pos + Vec*cos(alfa)*longitud_salto + osg::Vec3d(0, 0, sin(alfa)*altura_salto);
				alfa = alfa - PI / 40;
				personaje->setMatrix(mtotal);
				_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
				if (alfa <= -PI / 4) { alfa = PI / 4; space1 = 0; }
			}
			else
			{
				LocationSegment1->set(Pos, Pos + osg::Vec3d(0, 0, -0.5));
				findElevationVisitor->addLineSegment(LocationSegment1);
				_grupo->accept(*findElevationVisitor);
				ElevationLocatorHits = findElevationVisitor->getHitList(LocationSegment1);
				
				if (ElevationLocatorHits.empty()){
					mtras = mtras.translate(Pos + osg::Vec3d(0, 0, -0.5));
					mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
					mtotal = mesc*mrotz*mtras;
					Pos = Pos + osg::Vec3d(0, 0, -0.5);
					personaje->setMatrix(mtotal);
					_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
				}
				else {
					osgUtil::Hit p = ElevationLocatorHits.front();
					osg::Vec3d terrainHeight = p.getWorldIntersectPoint();
					mtras = mtras.translate(terrainHeight+ osg::Vec3d(0, 0, 0.1));
					mrotz = mrotz.rotate(ang, osg::Vec3d(0, 0, 1));
					mtotal = mesc*mrotz*mtras;
					Pos = terrainHeight + osg::Vec3d(0, 0, 0.1);
					personaje->setMatrix(mtotal);
					_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
					space1 = 0;
					alfa = PI/4;
				}
				//osgUtil::Hit p = ElevationLocatorHits.front();
				//osg::Vec3d terrainHeight = p.getWorldIntersectPoint();
				//printf("%f",terrainHeight.x());
				//return true;
			}
	}
	if (handler->q == 1) {
			mrotz = mrotz.rotate(ang + PI / angulo_giro, osg::Vec3d(0, 0, 1));
			mtotal = mesc*mrotz*mtras;
			quat = osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), PI / angulo_giro, osg::Vec3d(0, 0, 1));
			Vec = quat*Vec;
			personaje->setMatrix(mtotal);
			_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
			ang = ang + PI / angulo_giro;
			//return true;
	}
	if (handler->e == 1) {
			//_driveCar->preMult(osg::Matrix::rotate(osg::DegreesToRadians(-10.0f),	osg::Z_AXIS));
			mrotz = mrotz.rotate(ang - PI / angulo_giro, osg::Vec3d(0, 0, 1));
			mtotal = mesc*mrotz*mtras;
			quat = osg::Quat(0, osg::Vec3d(1, 0, 0), 0, osg::Vec3d(0, 1, 0), -PI / angulo_giro, osg::Vec3d(0, 0, 1));
			Vec = quat*Vec;
			personaje->setMatrix(mtotal);
			_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
			ang = ang - PI / angulo_giro;
	}
	if (handler->inicio == 1) {
			mtras = mtras.translate(0, 0, 0);
			mrotz = mrotz.rotate(PI / 2, osg::Vec3d(0, 0, 1));
			mesc = mesc.scale(1, 1, 1);
			mtotal = mesc*mrotz*mtras;
			Pos = osg::Vec3d(0, 0, 0);
			Vec = osg::Vec3d(1, 0, 0);
			ang = PI / 2;
			personaje->setMatrix(mtotal);
			_viewer->getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));
	}
}