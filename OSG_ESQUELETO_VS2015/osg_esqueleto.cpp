﻿#pragma once
#include "stdafx.h"

#include "config_file.h"
#include "FuncionesAyuda.h"
#include "movimiento.h"
#include "MyKeyboardEventHandler.h"
//#include <iostream>



using namespace rapidxml;
//using namespace std;

#define PI 3.14159265358979323846
//1.57079632679489661923

int main(int, char**)
{
	config_file config("./Arbol_layout.xml");
	xml_document<> doc;    // character type defaults to char
	doc.parse<0>(config.buffer);    // 0 means default parse flags

	osgViewer::Viewer viewer;
	viewer.setUpViewInWindow(50, 50, 800, 600, 0);
	osg::Group * grupo = new osg::Group;
	osg::Group * grupo_total = new osg::Group;

	viewer.setSceneData(grupo_total);
	//SimulacionKeyboardEvent* KeyboardEvent = new SimulacionKeyboardEvent();
	//viewer.addEventHandler(KeyboardEvent);

	/*osg::Geode* malla = CreaMalla(512, 1);										       // Crea la malla del suelo
	AddTexture(malla, ".\\heightmap\\Texturas\\heightmap.png", 0);		               // Añade la textura a la malla
	AddTexture(malla, ".\\heightmap\\Texturas\\heightmap_normals.png", 1);		       // Añade la textura a la malla
	AddTexture(malla, ".\\heightmap\\Texturas\\Heightmap_rgb.png", 2);		           // Añade la textura a la malla
	AddTexture(malla, ".\\heightmap\\Texturas\\cesped.dds", 3);		                   // Añade la textura a la malla
	AddTexture(malla, ".\\heightmap\\Texturas\\snow.dds", 4);		                   // Añade la textura a la malla
	AddTexture(malla, ".\\heightmap\\Texturas\\mountain.dds", 5);		               // Añade la textura a la malla
	AddShader(malla, ".\\heightmap\\heightmap_sombras.vert", ".\\heightmap\\heightmap_sombras.frag");
	grupo->addChild(malla);*/
	//----------------------------- Luces ----------------------------------
	//Luz global
	osg::Light* myLight = new osg::Light;
	myLight->setLightNum(0);
	myLight->setPosition(osg::Vec4(-500.0, 500.0, 1000.0, 0.0f));				// Posicion foco
	myLight->setAmbient(osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f));				// Color luz ambiente
	myLight->setDiffuse(osg::Vec4(0.5f, 0.5f, 0.5f, 1.0f));				// Color luz difusa
	myLight->setDirection(osg::Vec3(0.1f, -0.1f, -0.2f));					// Direccion

	osg::LightSource* lightS = new osg::LightSource;
	lightS->setLight(myLight);
	lightS->setLocalStateSetModes(osg::StateAttribute::ON);

	osg::Group * grupo_luz = new osg::Group;
	grupo_luz->addChild(lightS);
	grupo_total->addChild(grupo_luz);

	//------------------------------ Sombras ------------------------------------	

	osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;


	float minLightMargin = 10.f;
	float maxFarPlane = 200;
	unsigned int texSize = 1024;
	unsigned int baseTexUnit = 0;
	unsigned int shadowTexUnit = 6;

	texSize =8192;
	sm->setLight(myLight);
	sm->setMinLightMargin(minLightMargin);
	sm->setMaxFarPlane(maxFarPlane);
	sm->setTextureSize(osg::Vec2s(texSize, texSize));
	sm->setShadowTextureCoordIndex(shadowTexUnit);
	sm->setShadowTextureUnit(shadowTexUnit);
	sm->setBaseTextureCoordIndex(baseTexUnit);
	sm->setBaseTextureUnit(baseTexUnit);


	sc->setShadowTechnique(sm);

	const int ReceivesShadowTraversalMask = 0x1;
	const int CastsShadowTraversalMask = 0x2;

	sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
	sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);

	grupo_total->addChild(sc);
	sc->addChild(grupo);



	//--------------------------- Camara_texto ------------------------------- 
	osg::Camera* camera_texto = new osg::Camera;
	camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_texto->setViewMatrix(osg::Matrix::identity());
	camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_texto->setRenderOrder(osg::Camera::POST_RENDER);
	grupo_total->addChild(camera_texto);

	osg::Matrixd mtras, mrotz, mesc, mtotal;
	/*osg::MatrixTransform* texto = new osg::MatrixTransform;
	texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto1.OSGB"));
	mtras = mtras.translate(0, -20, 0);
	mrotz = mrotz.rotate(-PI/2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(4, 4, 4);
	mtotal = mesc*mrotz*mtras;
	texto->setMatrix(mtotal);
	camera_texto->addChild(texto);*/

	//----------------------------- Texto ----------------------------
	/*osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("fonts/arial.ttf");
	osg::Geode* geode_texto_intro = new osg::Geode();
	osgText::Text* texto_intro = new osgText::Text;
	geode_texto_intro->addDrawable(texto_intro);
	camera_texto->addChild(geode_texto_intro);

	texto_intro->setFont(font);
	texto_intro->setPosition(osg::Vec3(0.0f,0.0f,0.0f));
	texto_intro->setFontResolution(40,40);
	texto_intro->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro->setCharacterSize(20);
	texto_intro->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro->setText("Este es el texto que se muestra");*/



	osg::Node * nodo = NULL;

	auto node_base = doc.first_node();
	xml_node<>* node;
	osg::Node* fichero;
	float x, y, escala, angulo, z = 0;
	int i = 0, estado = 0, _switch;
	//osg::Matrixd mtras, mrotz, mesc, mtotal;
	osg::Image* img_heightmap = osgDB::readImageFile(".\\heightmap\\Texturas\\heightmap.png");
	osg::MatrixTransform *array[20];


	for (xml_node<> *node = node_base->first_node(); node != NULL; node = node->next_sibling())
	{
		for (xml_attribute<> *attr = node->first_attribute(); attr != NULL; attr = attr->next_attribute())
		{
			if (strcmp(attr->name(), "Fichero") == 0) {
				fichero = osgDB::readNodeFile(attr->value()); // te crea un nodo fichero
			}
			if (strcmp(attr->name(), "Grupo") == 0) {
				estado = atoi(attr->value());
			}
			if (strcmp(attr->name(), "escala") == 0) {
				escala = atof(attr->value());
			}
			if (strcmp(attr->name(), "x") == 0) {
				x = atof(attr->value());
			}
			if (strcmp(attr->name(), "y") == 0) {
				y = atof(attr->value());
			}
			if (strcmp(attr->name(), "angulo") == 0) {
				angulo = atof(attr->value());
			}

		}

		array[i] = new osg::MatrixTransform;
		array[i]->addChild(fichero);


		//z = obtenerAltura(x, y, img_heightmap, 512, 1, 100);

		mtras = mtras.translate(x, y, z);
		mrotz = mrotz.rotate(angulo, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(escala, escala, escala);
		mtotal = mesc*mrotz*mtras;
		array[i]->setMatrix(mtotal);	//metes la matriz con los valores en tu matriz nodo

		if (estado == 1) grupo->addChild(array[i]);

		i++;
		//cout << i;

	}

	osg::Switch * sw_maton = new osg::Switch;
	sw_maton->addChild(array[3]);
	sw_maton->addChild(array[4]);
	sw_maton->addChild(array[5]);
	sw_maton->setSingleChildOn(0);






	//osgDB::writeNodeFile(*grupo_total, "Borrar.osg");

	osg::MatrixTransform* mt2 = new osg::MatrixTransform;

	mt2->addChild(osgDB::readNodeFile(".\\modelos\\Boveda\\Boveda.OSG"));
	grupo->addChild(mt2);
	mtras = mtras.translate(0, 0, -5);
	mrotz = mrotz.rotate(PI/4, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(10, 10, 10);
	mtotal = mesc*mrotz*mtras;
	mt2->setMatrix(mtotal);
	mt2->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);
	array[1]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //personaje
	array[2]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //lord culombio
	array[3]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //maton
	array[4]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //maton_golpe
	array[5]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //maton_guardia
	array[6]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //prisionero
	array[7]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);  //maton
	array[8]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);
	array[9]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);
	array[12]->setNodeMask(mt2->getNodeMask() & ~CastsShadowTraversalMask);
	//mt1->preMult(osg::Matrix::rotate(osg::DegreesToRadians(-90.0f),
		//osg::Y_AXIS));

	viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(-60, 160, 1), osg::Vec3d(-40, 160, 0), osg::Vec3d(0, 0, 1));
	osgViewer::Viewer* punt = &viewer;

	MyKeyboardEventHandler* handler;
	handler = new MyKeyboardEventHandler;
	viewer.addEventHandler(handler);

	//viewer.addEventHandler(new MyKeyboardEventHandler);

	movimiento mov;
	mov._grupo = grupo;

	/*osg::Node * nodo = (osgDB::readNodeFile("modelos/Humvee/Humvee.obj"));

	grupo->addChild(nodo);
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodo);
	grupo->addChild(mt);
	osg::Switch * sw = new osg::Switch;
	osg::MatrixTransform* mt2 = new osg::MatrixTransform;
	grupo->addChild(mt2);
	sw->addChild(nodo);
	osg::MatrixTransform* mt3 = new osg::MatrixTransform;
	mt3->addChild(nodo);
	sw->addChild(mt3);
	osg::Matrixd mtras2;
	mtras2.translate(20, 0, 0);
	mt2->setMatrix(mtras2);
	osg::Matrixd mtras3;
	mtras3.scale(2, 2, 2);
	mt3->setMatrix(mtras3);

	osg::Matrixd mtras, mrotz, mrotx, mroty, mesc, mtotal;
	double tiempo;
	double ang = 3.1416 / 2.0;*/

	viewer.realize();

	/*osgGA::NodeTrackerManipulator::TrackerMode trackerMode = osgGA::NodeTrackerManipulator::NODE_CENTER_AND_ROTATION;
	osgGA::NodeTrackerManipulator::RotationMode rotationMode = osgGA::NodeTrackerManipulator::TRACKBALL;
	osg::ref_ptr<osgGA::NodeTrackerManipulator> tm = new osgGA::NodeTrackerManipulator;
	tm->setTrackerMode(trackerMode);
	tm->setRotationMode(rotationMode);
	tm->setMinimumDistance(50.0);
	tm->setTrackNode(mt1);

	osg::ref_ptr<osgGA::KeySwitchMatrixManipulator> keyswitchManipulator = new osgGA::KeySwitchMatrixManipulator;
	if (tm.valid()) keyswitchManipulator->addMatrixManipulator('0', "NodeTracker", tm.get());
	keyswitchManipulator->addMatrixManipulator('1', "Trackball", new osgGA::TrackballManipulator());;
	viewer.setCameraManipulator(keyswitchManipulator.get());*/



	/*while (!viewer.done()) {

		tiempo = ::GetTickCount();
		mtras = mtras.translate(osg::Vec3d(10 * cos(ang*tiempo / 1000.0), 10 * sin(ang*tiempo / 1000.0), 0));
		mrotz = mrotz.rotate(ang* tiempo / 1000.0, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(2, 1, 1);
		mtotal = mesc*mrotz*mtras;
		mt->setMatrix(mtotal);
		Sleep(33);
		viewer.frame();
	}*/

	//viewer.realize();

	//------------------------Carga de textos inicio y pelea, HUD------------------------------

	mtras = mtras.translate(0, -45, 0);
	mrotz = mrotz.rotate(PI / 2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(10.5, 10.5, 10);
	mtotal = mesc*mrotz*mtras;
	osg::Switch * swinicio = new osg::Switch;

	osg::MatrixTransform* mt3 = new osg::MatrixTransform;
	mt3->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Inicio1.OSGB"));
	mt3->setMatrix(mtotal);
	swinicio->addChild(mt3);
	osg::MatrixTransform* mt4 = new osg::MatrixTransform;
	mt4->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Inicio2.OSGB"));
	mt4->setMatrix(mtotal);
	swinicio->addChild(mt4);
	osg::MatrixTransform* mt5 = new osg::MatrixTransform;
	mt5->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Inicio3.OSGB"));
	mt5->setMatrix(mtotal);
	swinicio->addChild(mt5);
	osg::MatrixTransform* mt6 = new osg::MatrixTransform;
	mt6->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Inicio4.OSGB"));
	mt6->setMatrix(mtotal);
	swinicio->addChild(mt6);

	swinicio->setAllChildrenOff();
	camera_texto->addChild(swinicio);

	mtras = mtras.translate(0, -30, 0);
	mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(8, 8, 8);
	mtotal = mesc*mrotz*mtras;
	osg::Switch * swpelea = new osg::Switch;
	
	osg::MatrixTransform* mt7 = new osg::MatrixTransform;
	mt7->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\W.OSGB"));
	mt7->setMatrix(mtotal);
	swpelea->addChild(mt7);
	osg::MatrixTransform* mt8 = new osg::MatrixTransform;
	mt8->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\A.OSGB"));
	mt8->setMatrix(mtotal);
	swpelea->addChild(mt8);
	osg::MatrixTransform* mt9 = new osg::MatrixTransform;
	mt9->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\S.OSGB"));
	mt9->setMatrix(mtotal);
	swpelea->addChild(mt9);
	osg::MatrixTransform* mt10 = new osg::MatrixTransform;
	mt10->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\D.OSGB"));
	mt10->setMatrix(mtotal);
	swpelea->addChild(mt10);

	swpelea->setAllChildrenOff();
	camera_texto->addChild(swpelea);

	osg::Switch * swvida = new osg::Switch;

	osg::MatrixTransform* mt11 = new osg::MatrixTransform;
	mt11->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Vida.OSGB"));
	mtras = mtras.translate(200, -200, 0);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(0.8, 0.8, 0.8);
	mtotal = mesc*mrotz*mtras;
	mt11->setMatrix(mtotal);
	swvida->addChild(mt11);
	osg::MatrixTransform* mt12 = new osg::MatrixTransform;
	mt12->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Vida.OSGB"));
	mtras = mtras.translate(270, -200, 0);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(0.8, 0.8, 0.8);
	mtotal = mesc*mrotz*mtras;
	mt12->setMatrix(mtotal);
	swvida->addChild(mt12);
	osg::MatrixTransform* mt13 = new osg::MatrixTransform;
	mt13->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Vida.OSGB"));
	mtras = mtras.translate(340, -200, 0);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(0.8, 0.8, 0.8);
	mtotal = mesc*mrotz*mtras;
	mt13->setMatrix(mtotal);
	swvida->addChild(mt13);
	
	swvida->setAllChildrenOff();
	camera_texto->addChild(swvida);


	float x1 = 40;
	float y1 = 0;
	float z1 = 0;
	float tiempo = 1.0;
	float tiempo1 = 0.0;
	int dentro = 1;
	int tecla = 0;
	int l = 1;
	int tecla1 = 0;
	int tecla2 = 0;
	int vida = 3;
	int dentro1 = 0;
	int golpe = 0;
	float x2 = 0; //para mover al guardia
	float x3 = 0; //para guardar la coordenada x del personaje
	float y3 = 0; //para guardar la coordenada y del personaje
	float z3 = 0;
	int giro = 0; //para que el guardia gira
	int dentroc = 0;
	int teclac = 0;
	int dentroc2 = 0;
	int teclac2 = 0;
	int dentroc3 = 0;
	int teclac3 = 0;


	osg::Matrixd mtrasp, mrotzp, mescp, mtotalp;
	osg::Matrixd mtrasm, mrotzm, mescm, mtotalm;
	osg::Matrixd mtrasp2, mrotzp2, mescp2, mtotalp2; //preso
	osg::Matrixd mtrasr, mrotzr, mescr, mtotalr; //rata
	osg::Matrixd mtrasc, mrotzc, mescc, mtotalc; //carcel
	osg::Matrixd mtrasg, mrotzg, mescg, mtotalg; //guardia
	osg::Matrixd mtraspos, mrotzpos, mescpos, mtotalpos; //poster
	osg::Matrixd mtrastun, mrotztun, mesctun, mtotaltun; //tunel
	osg::Vec3d direccion_vista;

	mescp = mescp.scale(1, 1, 1);
	mescm = mescm.scale(0.8, 0.8, 0.8);

	//------------------------Menú inicio--------------------------

	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), mov.Pos, osg::Vec3d(0, 0, 1));
		if (l == 1) {
			swinicio->setSingleChildOn(0);
			if (handler->enter == 1) tecla = 1;
			else if (tecla == 1) dentro = 0;
		}
		if (l == 2) {
			swinicio->setSingleChildOn(1);
		}
		if (l == 3) {
			swinicio->setSingleChildOn(2);
		}
		if (l == 4) {
			swinicio->setSingleChildOn(3);
			if (handler->enter == 1) return true;
		}
		viewer.frame();
		if (handler->flecha_arriba == 1 && l >= 2 && tecla2 == 0) { tecla2 = 1; l = l - 1; }
		else if (handler->flecha_arriba == 0 && tecla2 == 1 && l >= 2) { tecla2 = 0; }
		if (handler->flecha_abajo == 1 && l <= 3 && tecla1 == 0) { tecla1 = 1; l = l + 1; }
		else if (handler->flecha_abajo == 0 && tecla1 == 1 && l <= 3) { tecla1 = 0; }
	}
	swinicio->setAllChildrenOff();
	camera_texto->removeChild(swinicio);
	
	//--------------------Historia inicio-------------------
	dentro = 1;
	tecla = 0;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Presentacion1.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, -1));
		mesc = mesc.scale(10.5, 10.5, 10);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->enter == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Presentacion2.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, -1));
		mesc = mesc.scale(10.5, 10.5, 10);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->enter == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Presentacion3.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, -1));
		mesc = mesc.scale(10.5, 10.5, 10);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->enter == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Presentacion4.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, -1));
		mesc = mesc.scale(10.5, 10.5, 10);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->enter == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}


	//--------------------Movimiento de camara--------------------
	x1 = 0;
	y1 = 170;
	z1 = 1;

	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(-20, 175, 2), mov.Pos + osg::Vec3d(x1, 0, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto0.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}

	while (!viewer.done() && z1<=8.5) {
		if (x1 <= 50) {
			punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(-20, 175, 2), mov.Pos + osg::Vec3d(x1, 0, 1), osg::Vec3d(0, 0, 1));
			x1 = x1 + 0.15;
		}
		else {
			punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(-20, 175, 2), mov.Pos + osg::Vec3d(x1, 0, z1), osg::Vec3d(0, 0, 1));
			z1 = z1 + 0.1;
		}
		viewer.frame();
	}
	Sleep(1000);


	//--------------------Movimiento libre-------------------
	punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - mov.Vec*20 + osg::Vec3d(0, 0, 1), mov.Pos, osg::Vec3d(0, 0, 1));
	swvida->setAllChildrenOn();

	while (!viewer.done() && mov.pos == 0) {
		//viewer.getCamera()->setViewMatrixAsLookAt(mt1->getMatrix().getTrans()+ osg::Vec3d(-5, 0, 5), mt1->getMatrix().getTrans(), osg::Vec3d(0, 0, 1));
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}

	//---------------------Textos-----------------------
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto1.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	grupo->addChild(sw_maton);
	mtrasm = mtrasm.translate(40, -95, 0);
	mrotzm = mrotzm.rotate(PI / 2, osg::Vec3d(0, 0, 1));
	mescm = mescm.scale(0.8, 0.8, 0.8);
	mtotalm = mescm*mrotzm*mtrasm;
	array[3]->setMatrix(mtotalm);
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(40, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto2.OSGB"));
		mtras = mtras.translate(90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}


	//---------------------Movimiento maton---------------------

	x1 = 40;
	dentro = 1;
	mrotzm = mrotzm.rotate(PI / 2, osg::Vec3d(0, 0, 1));
	mescm = mescm.scale(0.8, 0.8, 0.8);
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(x1, -95, 1), osg::Vec3d(0, 0, 1));
		if (x1 < 57) {
			mtrasm = mtrasm.translate(x1, -95, 0);
			mtotalm = mescm*mrotzm*mtrasm;
			array[3]->setMatrix(mtotalm);
			x1 = x1 + 0.1;
			if (x1 >= 57) { mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1)); mtras = mtras.translate(60, -95, 0); mesc = mesc.scale(0.8, 0.8, 0.8); mtotal = mesc*mrotz*mtras; array[6]->setMatrix(mtotal); dentro = 0; }
		}
		viewer.frame();
	}

	//-----------------------Textos----------------------

	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto3.OSGB"));
		mtras = mtras.translate(-90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto4.OSGB"));
		mtras = mtras.translate(90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}

	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto5.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}


	//-----------------------------Movimiento libre------------------------

	while (!viewer.done() && mov.pos != 2) {
		//viewer.getCamera()->setViewMatrixAsLookAt(mt1->getMatrix().getTrans()+ osg::Vec3d(-5, 0, 5), mt1->getMatrix().getTrans(), osg::Vec3d(0, 0, 1));
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}

	//-----------------------------Textos------------------------

	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto6.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}

	direccion_vista = mov.Pos - osg::Vec3d(57.0, -95.0, 0.0);
	if (direccion_vista._v[1] >= 0) {
		mrotzm = mrotzm.rotate(PI / 2 + acos(direccion_vista._v[0] / sqrt(direccion_vista._v[0] * direccion_vista._v[0] + direccion_vista._v[1] * direccion_vista._v[1])), osg::Vec3d(0, 0, 1));
	}
	else {
		mrotzm = mrotzm.rotate(PI / 2 - acos(direccion_vista._v[0] / sqrt(direccion_vista._v[0] * direccion_vista._v[0] + direccion_vista._v[1] * direccion_vista._v[1])), osg::Vec3d(0, 0, 1));
	}
	mtotalm = mescm*mrotzm*mtrasm;
	array[3]->setMatrix(mtotalm);
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto7.OSGB"));
		mtras = mtras.translate(90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto8.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}

	tecla = 0;
	dentro = 1;
	l = 0;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), osg::Vec3d(60, -95, 1), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto9.OSGB"));
		mtras = mtras.translate(90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}


	//-----------------------------Posicionar para pelea------------------

	mtrasp = mtrasp.translate(42, -95, 0);
	mrotzp = mrotzp.rotate(PI, osg::Vec3d(0, 0, 1));
	mtotalp = mescp*mrotzp*mtrasp;
	array[1]->setMatrix(mtotalp);
	mov.Pos = osg::Vec3d(42, -95, 0);
	mov.Vec = osg::Vec3d(0, 1, 0);
	mov.ang = PI;
	mtrasm = mtrasm.translate(42, -91, 0);
	mrotzm = mrotzm.rotate(0, osg::Vec3d(0, 0, 1));
	mtotalm = mescm*mrotzm*mtrasm;
	array[3]->setMatrix(mtotalm);
	punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(42, -100, 2), osg::Vec3d(42, -91, 2.5), osg::Vec3d(0, 0, 1));


	//------------------------Movimiento camara pelea----------------------

	x1 = 42;
	z1 = 2;

	while (!viewer.done() && z1 <= 2.5) {
		if (x1 <= 45) {
			punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(x1, -100, 2), osg::Vec3d(42, -91, 2.5), osg::Vec3d(0, 0, 1));
			x1 = x1 + 0.05;
		}
		else {
			punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(x1, -100, z1), osg::Vec3d(42, -91, 2.5), osg::Vec3d(0, 0, 1));
			z1 = z1 + 0.05;
		}
		viewer.frame();
	}

	//---------------------------Explicacion de la pelea-----------------------------

	//texto de explicacion

	//--------------------------------Pelea------------------------------------------
	
	Sleep(500);
	array[4]->setMatrix(mtotalm);
	array[5]->setMatrix(mtotalm);
	l = 1;
	tecla = 0;
	dentro1 = 0;
	golpe = 0;
	dentro = 0;
	while (!viewer.done() && l <= 9) {
		if (dentro1 == 0) {
			srand(time(NULL));
			golpe = rand() % 4;
			dentro1 = 1;
		}
		if (golpe == 0 && dentro1 == 1) {
			if (dentro == 0) {
				tiempo = GetTickCount();
				dentro = 1;
			}
			sw_maton->setSingleChildOn(1);
			if (tecla == 0) swpelea->setSingleChildOn(2);
			if (handler->s == 1 && tecla == 0) {
				tecla = 1; l = l + 1; swpelea->setAllChildrenOff();
			}
			else if (handler->s == 0 && tecla == 1) {
				tecla = 0; dentro1 = 0; dentro = 0; Sleep(500);
			}
			if (GetTickCount() > tiempo + 2500) {
				vida = vida - 1;
				if (vida==2) swvida->setValue(0, 0);
				else  swvida->setValue(1, 0);
				dentro = 0;
				dentro1 = 0;
				swpelea->setAllChildrenOff();
				Sleep(500);
			}
		}
		else if (golpe == 1 && dentro1 == 1) {
			if (dentro == 0) {
				tiempo = GetTickCount();
				dentro = 1;
			}
			sw_maton->setSingleChildOn(2);
			if (tecla == 0) swpelea->setSingleChildOn(0);
			if (handler->w == 1 && tecla == 0) {
				tecla = 1; l = l + 1; swpelea->setAllChildrenOff();
			}
			else if (handler->w == 0 && tecla == 1) {
				tecla = 0; dentro1 = 0; dentro = 0; Sleep(500);
			}
			if (GetTickCount() > tiempo + 2500) {
				vida = vida - 1;
				if (vida == 2) swvida->setValue(0, 0);
				else  swvida->setValue(1, 0);
				dentro = 0;
				dentro1 = 0;
				swpelea->setAllChildrenOff();
				Sleep(500);
			}
		}
		else if (golpe == 2 && dentro1 == 1) {
			if (dentro == 0) {
				tiempo = GetTickCount();
				dentro = 1;
			}
			sw_maton->setSingleChildOn(1);
			if (tecla == 0) swpelea->setSingleChildOn(1);
			if (handler->a == 1 && tecla == 0) {
				tecla = 1; l = l + 1; swpelea->setAllChildrenOff();
			}
			else if (handler->a == 0 && tecla == 1) {
				tecla = 0; dentro1 = 0; dentro = 0; Sleep(500);
			}
			if (GetTickCount() > tiempo + 2500) {
				vida = vida - 1;
				if (vida == 2) swvida->setValue(0, 0);
				else  swvida->setValue(1, 0);
				dentro = 0;
				dentro1 = 0;
				swpelea->setAllChildrenOff();
				Sleep(500);
			}
		}
		else if (golpe == 3 && dentro1 == 1) {
			if (dentro == 0) {
				tiempo = GetTickCount();
				dentro = 1;
			}
			sw_maton->setSingleChildOn(1);
			if (tecla == 0) swpelea->setSingleChildOn(3);
			if (handler->d == 1 && tecla == 0) {
				tecla = 1; l = l + 1; swpelea->setAllChildrenOff();
			}
			else if (handler->d == 0 && tecla == 1) {
				tecla = 0; dentro1 = 0; dentro = 0; Sleep(500);
			}
			if (GetTickCount() > tiempo + 2500) {
				vida = vida - 1;
				if (vida == 2) swvida->setValue(0, 0);
				else  swvida->setValue(1, 0);
				dentro = 0;
				dentro1 = 0;
				swpelea->setAllChildrenOff();
				Sleep(500);
			}
		}
		if (vida == 0) return true;
		viewer.frame();
	}

	sw_maton->setSingleChildOn(0);
	swpelea->setAllChildrenOff();
	Sleep(1000);
	
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(x1 + 5, -105, z1), osg::Vec3d(42, -91, 2.5), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto10.OSGB"));
		mtras = mtras.translate(90, 170, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}

	//-----------------------Persecucion-------------------
	x1 = 42;
	y1 = -91;
	mrotzm = mrotzm.rotate(6*PI/5, osg::Vec3d(0, 0, 1));
	while (!viewer.done() && y1 <= -60) {
		mtrasm = mtrasm.translate(x1, y1, 0);
		mtotalm = mescm*mrotzm*mtrasm;
		array[3]->setMatrix(mtotalm);
		y1 = y1 + 0.25;
		x1 = x1 - 0.08;
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(10, 0, 1), array[3]->getMatrix().getTrans(), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto11.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	mrotzm = mrotzm.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
	while (!viewer.done() && x1>=-80) {
		mtrasm = mtrasm.translate(x1, y1, 0);
		mtotalm = mescm*mrotzm*mtrasm;
		array[3]->setMatrix(mtotalm);
		if(x1>=0) x1 = x1 - 0.25;
		else x1 = x1 - 0.3;
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}
	mrotzm = mrotzm.rotate(-PI / 4, osg::Vec3d(0, 0, 1));
	while (!viewer.done() && y1>=-80) {
		mtrasm = mtrasm.translate(x1, y1, 0);
		mtotalm = mescm*mrotzm*mtrasm;
		array[3]->setMatrix(mtotalm);
		y1 = y1 - 0.15;
		if (x1 >= -102) x1 = x1 - 0.3;
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}
	mtras = mtras.translate(x1, y1, 0);
	mesc = mesc.scale(0.4, 0.4, 0.4);
	mrotz = mrotz.rotate(PI, osg::Vec3d(0, 0, 1));
	mtotal = mesc*mrotz*mtras;
	array[2]->setMatrix(mtotal);
	grupo->addChild(array[2]);
	mtrasm = mtrasm.translate(x1+2, y1, 0);
	mrotzm = mrotzm.rotate(PI, osg::Vec3d(0, 0, 1));
	mtotalm = mescm*mrotzm*mtrasm;
	array[3]->setMatrix(mtotalm);
	mtrasm = mtras.translate(x1-2, y1, 0);
	mtotalm = mescm*mrotzm*mtrasm;
	array[7]->setMatrix(mtotalm);
	grupo->addChild(array[7]);

	dentro = 1;
	while (!viewer.done() && dentro==1) {
		if (mov.Pos._v[0] >= -107 && mov.Pos._v[0] <= -97 && mov.Pos._v[1] >= -89 && mov.Pos._v[1] <= -71) dentro = 0;
		tiempo1 = GetTickCount();
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		tiempo = GetTickCount() - tiempo1;
	}

	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos-mov.Vec*20 + osg::Vec3d(-11, 0, 4), array[3]->getMatrix().getTrans(), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto12.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	tecla = 0;
	dentro = 1;
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - mov.Vec * 20 + osg::Vec3d(-11, 0, 4), array[3]->getMatrix().getTrans(), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Texto13.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		if (handler->f == 1) tecla = 1;
		else if (tecla == 1) dentro = 0; camera_texto->removeChild(texto);
	}
	dentro = 1;
	tecla = 0;
	swvida->setAllChildrenOff();
	while (!viewer.done() && dentro == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * 20) + osg::Vec3d(0, 0, 1), mov.Pos, osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\Inconsciente.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, -1));
		mesc = mesc.scale(10.5, 10.5, 10);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		viewer.frame();
		Sleep(7000);
		mov.carcel = 1;
		dentro = 0;
		camera_texto->removeChild(texto);
	}
	
	

	//-----------------------Entrada en la cárcel-------------------
	mtras = mtras.translate(295, 165, 0);
	mrotz = mrotz.rotate(PI/2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(1, 1, 1);
	mtotal = mesc*mrotz*mtras;
	array[1]->setMatrix(mtotal);
	mov.Pos = osg::Vec3d(295, 165, 0);
	grupo->removeChild(array[0]);
	///carcel
	mtrasc = mtrasc.translate(300, 160, 0);
	mrotzc = mrotzc.rotate(0, osg::Vec3d(0, 0, 1));
	mescc = mescc.scale(0.3, 0.3, 0.3);
	mtotalc = mescc*mrotzc*mtrasc;
	array[9]->setMatrix(mtotalc);
	grupo->addChild(array[9]);
	////prisionero
	mtrasp2 = mtrasp2.translate(297, 166, 0);
	mrotzp2 = mrotzp2.rotate(0, osg::Vec3d(0, 0, 1));
	mescp2 = mescp2.scale(0.8, 0.8, 0.8);
	mtotalp2 = mescp2*mrotzp2*mtrasp2;
	array[6]->setMatrix(mtotalp2);
	grupo->addChild(array[6]);
	////guardia
	mtrasg = mtrasg.translate(300, 157, 0);
	mrotzg = mrotzg.rotate(PI , osg::Vec3d(0, 0, 1));
	mescg = mescg.scale(0.8, 0.8, 0.8);
	mtotalg = mescg*mrotzg*mtrasg;
	array[8]->setMatrix(mtotalg);
	grupo->addChild(array[8]);
	x2 = 294;
	////poster
	mtraspos = mtraspos.translate(298.1, 163.7, 2.4);
	mrotzpos = mrotzpos.rotate(0, osg::Vec3d(0, 0, 1));
	mescpos = mescpos.scale(0.03, 0.03, 0.03);
	mtotalpos = mescpos*mrotzpos*mtraspos;
	array[11]->setMatrix(mtotalpos);
	grupo->addChild(array[11]);



	mov.alturaLineSegment = osg::Vec3d(0, 0, 0.2);
	mov.altura_salto = 0.6;
	mov.angulo_giro = 160;
	mov.vel1 = mov.vel1*2/3;
	dentroc = 1;
	mov.carcel = 1;

	while (!viewer.done() && (mov.carcel == 1) && (dentroc == 1)) {
		//tiempo1 = GetTickCount();

		
		//mov.distancia_camara = 9;
		//mov.altura_camara = osg::Vec3d(0, 0, 3.5);

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(300, 165, 2.5), osg::Vec3d(300, 164, 2.5), osg::Vec3d(0, 0, 1));
		
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel1.OSGB"));
		mtras = mtras.translate(0, -180, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);


		
		//>getCamera()->setViewMatrixAsLookAt(Pos - (Vec * distancia_camara) + altura_camara, Pos, osg::Vec3d(0, 0, 1));

		viewer.frame();

		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0; camera_texto->removeChild(texto);

	}
	swvida->setAllChildrenOn();
	teclac = 0;
	dentroc = 1;

	mov.distancia_camara = 15;
	mov.altura_camara = osg::Vec3d(0, 0, 15);
	punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * mov.distancia_camara) + mov.altura_camara, mov.Pos, osg::Vec3d(0, 0, 1));
	osg::MatrixTransform* texto = new osg::MatrixTransform;
	while (!viewer.done() && dentroc == 1) {


		mov.muevete(handler, array[1], punt, tiempo);

		//////////////////////movimiento del guarda///////////////////////////
		if (x2 < 308 && giro == 0) {
			mtrasg = mtrasg.translate(x2, 158, 0);
			mrotzg = mrotzg.rotate(PI / 2, osg::Vec3d(0, 0, 1));
			mtotalg = mescg*mrotzg*mtrasg;
			array[8]->setMatrix(mtotalg);
			x2 = x2 + 0.05;
			if (x2 > 306) {
				giro = 1;
			}
		}
		if (giro == 1) {

			mtrasg = mtrasg.translate(x2, 158, 0);
			mrotzg = mrotzg.rotate(3 * PI / 2, osg::Vec3d(0, 0, 1));
			mtotalg = mescg*mrotzg*mtrasg;
			array[8]->setMatrix(mtotalg);
			x2 = x2 - 0.05;
			if (x2 < 294) {
				giro = 0;
			}
		}

		/////////////////////conversación en la cárcel//////////////////////////

		x3 = mov.Pos._v[0];
		y3 = mov.Pos._v[1];

		if (x3 > 299 && y3 < 163 && dentroc2 == 0) {
			punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * mov.distancia_camara) + mov.altura_camara, mov.Pos, osg::Vec3d(0, 0, 1));

			texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel0.OSGB"));
			mtras = mtras.translate(0, -30, 0);
			mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
			mesc = mesc.scale(4, 4, 4);
			mtotal = mesc*mrotz*mtras;
			texto->setMatrix(mtotal);
			camera_texto->addChild(texto);
			dentroc2 = 1;

		}
		else if (dentroc2 == 1 && x3 < 298) {
			camera_texto->removeChild(texto);
			dentroc = 0;
		}

		viewer.frame();

	}
	dentroc = 1;

	mov.distancia_camara = 7;
	mov.altura_camara = osg::Vec3d(0, 0, 6);


	while (!viewer.done() && dentroc == 1) {
		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));
		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel2.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;

		camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel3.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0; camera_texto->removeChild(texto2);


	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel4.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);


	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel5.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel6.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel7.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel8.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;


	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel9.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel10.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;


	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel11.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;


	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel12.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;


	while (!viewer.done() && dentroc == 1) {

		punt->getCamera()->setViewMatrixAsLookAt(osg::Vec3d(302, 162, 5), osg::Vec3d(296, 166, 0), osg::Vec3d(0, 0, 1));		osg::MatrixTransform* texto2 = new osg::MatrixTransform;
		texto2->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel13.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto2->setMatrix(mtotal);
		camera_texto->addChild(texto2);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto2);

	}
	teclac = 0;
	dentroc = 1;
	grupo->removeChild(array[11]);

	////////////////////se quita el poster//////////

	while (!viewer.done() && dentroc == 1) {
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel14.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);


		mov.distancia_camara = 15;
		mov.altura_camara = osg::Vec3d(0, 0, 15);
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * mov.distancia_camara) + mov.altura_camara, mov.Pos, osg::Vec3d(0, 0, 1));
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto);

	}
	teclac = 0;
	dentroc = 1;

	while (!viewer.done() && dentroc == 1) {
		osg::MatrixTransform* texto = new osg::MatrixTransform;
		texto->addChild(osgDB::readNodeFile(".\\modelos\\Textos\\TextosCarcel15.OSGB"));
		mtras = mtras.translate(0, -30, 0);
		mrotz = mrotz.rotate(-PI / 2, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(4, 4, 4);
		mtotal = mesc*mrotz*mtras;
		texto->setMatrix(mtotal);
		camera_texto->addChild(texto);
		mov.distancia_camara = 15;
		mov.altura_camara = osg::Vec3d(0, 0, 15);
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * mov.distancia_camara) + mov.altura_camara, mov.Pos, osg::Vec3d(0, 0, 1));
		mov.muevete(handler, array[1], punt, tiempo);
		viewer.frame();
		if (handler->f == 1) teclac = 1;
		else if (teclac == 1) dentroc = 0;	camera_texto->removeChild(texto);

	}
	teclac = 0;
	dentroc = 1;


	while (!viewer.done() && dentroc == 1) {

		mov.muevete(handler, array[1], punt, tiempo);
		x3 = mov.Pos._v[0];
		y3 = mov.Pos._v[1];
		z3 = mov.Pos._v[2];
		if (x3 >= 298 && y3 < 166 && z3 >1) {
			dentroc = 0;
			grupo->removeChild(array[9]);
			grupo->removeChild(array[6]);
			grupo->removeChild(array[8]);

		}

		viewer.frame();
	}
	dentroc = 1;
	
	mov.Pos = osg::Vec3d(305, 180, 1.3); ///se coloca en el laberinto
	grupo->removeChild(array[9]);
	grupo->removeChild(array[6]);
	grupo->removeChild(array[8]);
	///laberinto

	mtrastun = mtrastun.translate(300, 160, -0.2);
	mrotztun = mrotztun.rotate(0, osg::Vec3d(0, 0, 1));
	mesctun = mesctun.scale(0.3, 0.3, 0.3);
	mtotaltun = mesctun*mrotztun*mtrastun;
	array[12]->setMatrix(mtotaltun);
	grupo->addChild(array[12]);

	////ratas

	mtrasr = mtrasr.translate(303.5, 195, 0);
	mrotzr = mrotzr.rotate(PI, osg::Vec3d(0, 0, 1));
	mescr = mescr.scale(0.2, 0.2, 0.2);
	mtotalr = mescr*mrotzr*mtrasr;
	array[10]->setMatrix(mtotalr);
	grupo->addChild(array[10]);

	mov.alturaLineSegment = osg::Vec3d(0, 0, 3);

	while (!viewer.done() && dentroc == 1) {
		mov.distancia_camara = 15;
		mov.altura_camara = osg::Vec3d(0, 0, 15);
		punt->getCamera()->setViewMatrixAsLookAt(mov.Pos - (mov.Vec * mov.distancia_camara) + mov.altura_camara, mov.Pos, osg::Vec3d(0, 0, 1));
		mov.muevete(handler, array[1], punt, tiempo);
		//////////////encuentro con rata
		x3 = mov.Pos._v[0];
		y3 = mov.Pos._v[1];
		z3 = mov.Pos._v[2];
		if (x3<304 && y3>= 194) {
			
		grupo->removeChild(array[10]);
			

		}
		viewer.frame();
	}

	
}